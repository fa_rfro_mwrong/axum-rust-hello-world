#!/usr/bin/env bash
HELP_OPT_REGEX="^(--help|-h)$"
read -r -d '' help_msg << EOM
docker_build.bash: Description
------
Usage:
    docker_build.bash <arg>
    docker_build.bash --help
EOM

if [[ "$1" =~ $HELP_OPT_REGEX ]]; then
    echo "$help_msg" >&2
    exit 1
fi

if [[ $(git diff --stat) != '' ]]; then
    echo 'Dirty work tree. Commit your work first before build.'
    exit 1
else
    commit_hash=$(git rev-parse --verify HEAD)
fi

datetag=$(date +"%Y%m%d.%H%M%S.%03N")
buildtag="$datetag-$commit_hash"

cargo build --release

docker login registry.gitlab.com

docker_repo="registry.gitlab.com/fa_rfro_mwrong/axum-rust-hello-world"
docker build -f docker/onestage.Dockerfile \
--cache-from "$docker_repo":latest \
--tag "$docker_repo":latest \
--tag "$docker_repo":"$buildtag" \
.
docker push --all-tags "$docker_repo"

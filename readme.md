# Dummy web app 

* Output can be set with the `HTTP_RESPONSE` env var
* Listening port can be set with the `PORT` env var. Default is 3000.
* URL path can be set with the `URL_PATH` env var. Default is `/hello`.

## Docker compose


```
hello1: 
  image: registry.gitlab.com/fa_rfro_mwrong/axum-rust-hello-world
  environment:
    HTTP_RESPONSE: "hello 1"
	PORT: 3300
	URL_PATH: hello1
```
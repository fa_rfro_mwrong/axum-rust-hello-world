FROM rust:1.55.0-slim-buster AS builder
WORKDIR /usr/src/app
COPY Cargo.toml Cargo.lock ./
COPY src ./src
RUN cargo install --path .

# Bundle Stage
FROM ubuntu:20.04
COPY --from=builder /usr/local/cargo/bin/helloworld .
USER 1000
CMD ["./helloworld"]

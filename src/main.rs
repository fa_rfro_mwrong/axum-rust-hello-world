use axum::{routing::get, response::Html, Router};
use std::env;
use std::net::SocketAddr;

#[tokio::main]
async fn main() {
    let text = env::var("HTTP_RESPONSE")
        .unwrap_or("hello there, this is the default response from axum hello demo.".to_string());
    let port: u16 = env::var("PORT").unwrap_or("3000".to_string()).parse().unwrap();
    // build our application with a route
	let url_path = env::var("URL_PATH").unwrap_or("/hello".to_string());
    let app = Router::new().route(
        &url_path,
        get(move || {
            return handler(text);
        }),
    );

    // run it
    let addr = SocketAddr::from(([0, 0, 0, 0], port));
    println!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn handler(text: String) -> Html<String> {
    let res = format!("<h1>{}</h1>", text);
    Html(res)
}
